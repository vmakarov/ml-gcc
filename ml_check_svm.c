/* Check prediction results of given model on a test file.  The test
   data should be scaled according to given svm scale file.

   Usage: <svm model file> <svm scale file> <test file>

   The program outputs the result of the prediction into stderr: in
   how many cases there is an improvement even if the prediction and
   the actual best result are different.
*/

#include <stdio.h>
#include <assert.h>
#define XNEWVEC(t, n) ((t*) malloc (sizeof (t) * (n)))
#include "ml_data_read.c"
#include <libsvm/svm.h>

static float *input;
static float *output;
static char **opts;

int main (int argc, const char *argv[])
{
  double impr;
  struct svm_node *test;
  struct svm_model *m;
  int i, j, row, inp, out, res, all_fails, all_alls, *fails, *alls;
  FILE *f;

  if (argc != 4) {
    fprintf (stderr, "Usage: %s <model file> <svm scale file> <test file>.\n", argv[0]); exit (1);
  }

  m = svm_load_model (argv[1]);
  read_scales (argv[2]);
  f = get_file (argv[3], "r");
  read_file (f, &input, &output, &opts, &row, &inp, &out);
  test = (struct svm_node *) get_mem ((inp + 1) * sizeof (struct svm_node));
  fails = (int *) get_mem (row * sizeof (int));
  alls = (int *) get_mem (row * sizeof (int));
  memset (fails, 0, row * sizeof (int));
  memset (alls, 0, row * sizeof (int));
  impr = 0.0;
  for (i = 0; i < row; i++) {
    scale_input (input + i * inp, inp);
    for (j = 0; j < inp; j++) {
      test[j].index = j + 1;
      test[j].value = input[i * inp + j];
    }
    test[j].index = -1;
    int n = svm_predict (m, test);
    n--;
    printf ("\n%4d: %d\n", i, n);
    if (n == -1) { // base flag
      impr += 1.0;
    } else {
      alls[n]++;
      impr += output[i * out + n];
      if (output[i * out + n] > 1.0)
	fails[n]++;
    }
  }
  for (all_fails = all_alls = i = 0; i < out; i++) {
    if (alls[i] != 0)
      fprintf (stderr, "%d: %d/%d (%d%%)\n",
	       i, fails[i], alls[i], fails[i] * 100 / alls[i]);
    all_fails += fails[i];
    all_alls += alls[i];
  }
  if (all_alls == 0)
    fprintf (stderr, "Overall: Only the base flags\n");
  else
    fprintf (stderr, "Overall: %d/%d (%d%%)\n",
	     all_fails, all_alls, all_fails * 100 / all_alls);
  fprintf (stderr, "Overall improvement: %g\n", impr / row);
  free (fails);
  free (alls);
  svm_free_model_content (m);
  free (rs);
  free (test);
  for (i = 0; i < out; i++) free (opts[i]);
  free (opts); free (input); free (output);
  
  return 0;
}
