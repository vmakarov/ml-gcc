// Usage: program <input file>
// File contains lines
// + "options" features

// We extract data from such lines and print it into our data format.
// See the format description in ml_data_read.c

if (#argv != 1) {putln ("Usage: program <input file>"); exit (1);}
var fn = argv[0];

func fexists (fn) {// return nonzero if file FN exists
  var f;
  try { f = open (fn, "r"); } catch (except) {return 0;}
  close (f); return 1;
}

if (! fexists (fn)) {putln ("Can not open file ", fn); exit (1);}

var f = open (fn, "r"), lns = fgetf (f, 1);
close (f);

func collect_set (lns) { // Remove lines without prefix "+ ", remove the prefix too.
  for (var i = 0; i < #lns; i++)
    if (lns[i][0] == '+' && lns[i][1] == ' ') {
      lns[i] = subv (lns[i], 2);
    } else {
      del (lns, i); i--;
    }
  return lns;
}

var opts_vec = [], opts_map = {}; // map: opts->index
// Map full fun name -> vector of [option index, runtime factor]
var runtime_map = {};

// Return pair [first line LN field, the rest of line]
func extract (ln) {
  for (var i = 0; i < #ln && ln[i] == ' '; i++) // skip blanks
    ;
  for (; i < #ln && ln[i] != ' '; i++) // skip non-blanks;
    ;
  return [subv (ln, 0, i), subv (ln, i)];
}

// Form opts_map and transform LN for the final format:
//    <run-time ratio> ... <feature> ...
func set_indexes (lns) {
  for (var i = 0; i < #lns; i++) {
    var opt_ind, ln = lns[i];
    if (ln[0] != '"') {putln ("Options absent"); exit (1);}
    for (var j = 1; j < #ln && ln[j] != '"' ; j++)
      ;
    if (j >= #ln) {putln ("Options unfinished"); exit (1);}
    var opts = subv (ln, 1, j - 1);
    if (opts in opts_map) opt_ind = opts_map{opts};
    else {
      opt_ind = #opts_map; opts_map{opts} = opt_ind; ins (opts_vec, opts, -1);
    }
    lns[i] = subv (ln, j + 1); // Remove option
    var fun, run_time_factor, p;
    p = extract (lns[i]); fun = p[0]; lns[i] = p[1]; // Extract full function name
    // Extract run-time factor:
    p = extract (lns[i]); lns[i] = p[1]; run_time_factor = float (p[0]);
    // Skip pass::fun
    p = extract (lns[i]); lns[i] = p[1];
    lns[i] = fun @ " " @ lns[i]; // fun @ features
    if (fun in runtime_map) { 
      ins (runtime_map{fun}, [opt_ind, run_time_factor], -1);
      del (lns, i); i--; // features already collected
    } else runtime_map{fun} = [[opt_ind, run_time_factor]];
  }
   // Transform vector of pairs [ind, ratio] into vector of ratios:
  for (i in runtime_map) {
    var els = runtime_map {i};
    var v = [#opts_map : 10.]; // initialize by default values
    for (var j = 0; j < #els; j++) v[els[j][0]] = els[j][1];
    runtime_map {i} = v;
  }
  // Form lines in final format: run-time ratio ... feature ...
  for (i = 0; i < #lns; i++) {
    var p = extract (lns[i]), fun = p[0];
    var v = runtime_map{fun}, ratios = "";
    lns[i] = p[1];
    for (var j = 0; j < #v; j++) ratios @= v[j] @ " ";
    lns[i] = ratios @ lns[i];
  }
  return lns;
}

func out_data (lns) {
  // Print header
  putln (#lns, " ", #split (lns[0]) - #opts_vec, " ", #opts_vec);
  // Print options
  for (var i = 0; i < #opts_vec; i++) putln (opts_vec[i]);
  // Print data lines
  for (i = 0; i < #lns; i++) putln (lns[i]);
}

func print_map (f) { // Used for debugging
  var i;
  for (i in opts_map) fputln (f, opts_map{i}, " ", i);
}

lns = collect_set (lns);
lns = set_indexes (lns);
out_data (lns);
//print_map (stderr);
