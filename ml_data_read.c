/* The file contains code to read training/testing data in format:
   <number of lines of data> <fn=features number> <no=options number>
   option set 0
   ...
   option set no-1
   <option set 0 ratio> ... <option set no-1 ration> <feature0> ... <feature fn-1>
   ...

   The first line is called a header.  Subsequent lines are option
   sets and then lines of run-time data.  The ratio on a data line is
   a run-time of a function compiled with the corresponding option set
   relative to run-time of one compiled with the base options.  The
   corresponding function features are at the rest of line.
   
   Here the features are also called input and the ratios are called
   output.
   
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Allocate memory of SIZE.  Return its address.  Exit if there is no
   more memory.  */
static void *
get_mem (size_t size) {
  void *m = (void *) XNEWVEC (char, size);
  if (!m) {fprintf (stderr, "Cannot allocate %d bytes.\n", (int) size); exit (1);}
  return m;
}

/* Open with mode M and return file FILENAME.  Exit if there is an
   error in opening.  */
static FILE *
get_file (const char *filename, const char *m)
{
  FILE *f = fopen (filename, m);
  if (!f) {fprintf (stderr, "Cannot open file %s.\n", filename); exit(1);}
  return f;
}

/* Read header in file FIN and return the fields through ROW, INP, OUT.
   Exit if there is an error in reading.  */
static void
read_header (FILE *fin, int *row, int *inp, int *out)
{
  if (fscanf (fin, "%d %d %d ", row, inp, out) != 3) {
    fprintf (stderr, "Wrong input file header.\n"); exit (1);
  }
}


/* Read whole file FIN and return the header fields through ROW, INP,
   OUT and options through OPTS (array of size OUT), features through
   INPUT (array of size ROWxINP) and ratios through OUTPUT (array of
   size ROWxOUT).  Exit if there is an error in reading.  */
static void
read_file (FILE *fin, float **input, float **output, char ***opts,
	   int *row, int *inp, int *out)
{
  float *d, *r;
  int c;
  int i, j;
  char o[10000];

  read_header (fin, row, inp, out);
  *opts = (char **) get_mem (*out * sizeof (char *));
  /* Skip options.  */
  for (i = 0; i < *out; i++) {
    for (j = 0; (c = fgetc (fin)) != '\n' && c != EOF; j++)
      o[j] = c;
    if (c == EOF) {
      fprintf (stderr, "Wrong options part of input file.\n"); exit (1);
    }
    o[j] = '\0';//fprintf (stderr, o);
    (*opts)[i] = (char*) get_mem (strlen (o) + 1);
    strcpy ((*opts)[i], o);
  }
  *input = d = (float*) get_mem(*row * *inp * sizeof(float));
  *output = r = (float*) get_mem(*row * *out * sizeof(float));
  for (i=0;i<*row;i++) {
    for (j=0;j<*out;++j) {
      fscanf(fin, "%g ", r); r++;
    }
    for (j=0;j<*inp;++j) {
      fscanf(fin, "%g ", d); d++;
    }
  }
  fclose(fin);
}

/* Minimal/maximal scaled value of features read from the scale file.
   Span is (rmax - rmin).  */
static float rmin, rmax, span;
/* Array of size 2 * FEATURES_NUM, in other words array of pairs of
   values whose 1st element of the pair is minimal value of the
   corresponding feature in training data set and the 2nd element is
   the maximal value.  This value is read from the scale file.  */
static float *rs;
static int scale_features_num;

/* Read the scale file FN and set up RMIN, RMAX, SPAN, and allocate
   and set up RS.  Return number of features gotten from the file.
   Exit if something wrong with the file.  */
static int
read_scales (const char *fn)
{
  FILE *f = get_file (fn, "r");
  int i;
  float r1, r2;
  
  if (fscanf (f, "%d %g %g ", &scale_features_num, &rmin, &rmax) != 3) {
    fprintf (stderr, "Wrong input file header %s.\n", fn); exit (1);
  }
  span = rmax - rmin;
  rs = (float *) get_mem (2 * scale_features_num * sizeof (float));
  for (i = 0; i < scale_features_num; i++)
    if (fscanf (f, "%g %g ", &r1, &r2) != 2) {
      fprintf (stderr, "Wrong input file header.\n"); exit (1);
    } else {
      rs[i * 2] = r1; rs[i * 2 + 1] = r2;
    }
  fclose (f);
  return scale_features_num;
}

/* Scale feature vector INPUT of size INP according to the scale
   file.  */
static void
scale_input (float *input, int inp)
{
  float v, mi, ma;
  int i;
  
  for (i = 0; i < inp; i++) {
    v = input[i]; mi = rs[i * 2], ma = rs[i * 2 + 1];
    /* The tail features with the same value might be absent in the
       file.  */
    v = i >= scale_features_num || mi == ma ? 0.0 : (v - mi) / (ma - mi) * span + rmin;
    input[i] = v;
  }
}
