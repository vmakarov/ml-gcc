// Script for trying different GCC options for different programs and
// getting their run-time characteristics and features.
//
// Usage: <program> <directories to process>
//
// Standard output: Log containing information about runs.  Run info
// and program features can be extracted from the log by another script.
//
// The script uses shell script "collect" to compile, run programs,
// and GCC plugin ml_features.so to get compiled function
// characteristics.  Collect should use perf and put specific
// temporary files.

include "pool";

var final COLLECT = "./collect"; // the script path relative to current dir
var final PLUGIN = "../ml_features.so"; // plugin path relative the
					// run program directories

// We can run more one collect script at once but we don't do it as it
// makes run-time characteristics inaccurate even on not busy
// multi-core CPU.  It is better wait more time to get more accurate
// characteristics.
var p = pool (1);

// Set up to directory currently being processed.
var curr_dir = nil;

// Base options which always present on GCC command line.
var base= "-O3 -w";

// Set of options we are trying.
var opts = [
"-fmodulo-sched",
"-fmodulo-sched -fmodulo-sched-allow-regmoves",
"-fschedule-insns",
"-fschedule-insns -fno-sched-group-heuristic",
//"-fschedule-insns -fno-critical-path-heuristic",
"-fschedule-insns -fno-sched-spec-insn-heuristic",
"-fschedule-insns -fno-sched-rank-heuristic",
"-fschedule-insns -fno-sched-last-insn-heuristic",
"-fschedule-insns -fno-sched-dep-count-heuristic",
"-fschedule-insns -fsched-pressure",
"-fschedule-insns -fsched-pressure -fno-sched-group-heuristic",
//"-fschedule-insns -fsched-pressure -fno-critical-path-heuristic",
"-fschedule-insns -fsched-pressure -fno-sched-spec-insn-heuristic",
"-fschedule-insns -fsched-pressure -fno-sched-rank-heuristic",
"-fschedule-insns -fsched-pressure -fno-sched-last-insn-heuristic",
"-fschedule-insns -fsched-pressure -fno-sched-dep-count-heuristic",
"-fno-schedule-insns2",
"-fno-sched-group-heuristic",
"-fno-critical-path-heuristic",
"-fno-sched-spec-insn-heuristic",
"-fno-sched-rank-heuristic",
"-fno-sched-last-insn-heuristic",
"-fno-sched-dep-count-heuristic",
"-fsched2-use-superblocks",
"-fsched2-use-traces",
"-fselective-scheduling",
"-fselective-scheduling -fsel-sched-pipelining",
"-fselective-scheduling2",
"-fselective-scheduling2 -fsel-sched-pipelining",
"-fsched-spec-load"
];

func fexists (fn) {// return nonzero if file FN exists
  var f;
  try { f = open (fn, "r"); } catch (except) {return 0;}
  close (f); return 1;
}

// Collect dirs to process, in other words, dirs containing compile
// and run script.
var dirs = new argv;
for (var i = 0; i < #dirs; i++) {
  if (fexists (dirs[i] @ "/compile"))
    continue;
  del (dirs, i); i--;
}

var cpid = getpid (); // Current process ID.
var outprefix = "/tmp/" @ cpid @ "_"; // Prefix for temporary files.

func collect (dir, opts) { // Collect run data for DIR with OPTS
  var outfn = outprefix @ dir;
  var p = popen (COLLECT @ " " @ dir @ " " @ outfn @ " " @ opts, "r");
  var lns;
  try {
    lns = fgetf (p, 1);
  } catch (invcalls.eof) {lns = [];}
  pclose (p); system ("rm -f " @ outfn);
  return lns;
}

func clean () { // Remove all temporary files which might exist
  if (curr_dir == nil) return;
  putln ("Cleaning");
  system ("rm -f "
	  @ outprefix @ curr_dir @ "* "
	  @ outprefix @ curr_dir @ "*.perf.data* "
	  @ outprefix @ "res_" @ curr_dir @ "* ");
}

// Return pair of execution and result file names for N_RUN.
func get_aout_and_res_names (n_run) {
  return [outprefix @ curr_dir @ n_run, outprefix @ "res_" @ curr_dir @ n_run];
}

// Return command to collect run-time characteristics and features to
// file RES of program AOUT created by using options OPTS.
func get_command (aout, res, opts) {
  return (COLLECT @ " " @ curr_dir @ " " @ aout @ " " @ opts
	  @ " -fplugin=" @ PLUGIN @ " >" @ res);
}

// Extend BASE by all options excluding EXCLUDE and add collect
// command with them to the pool for execution.  Each run will have
// unique number starting with N_RUN.  Return the number for
// subsequent execution.
func add_to_run (base, exclude, n_run) {
  for (var i = 0; i < #opts; i++)
    if (match (opts[i], base) == nil && ! (opts[i] in exclude)) {
	var t = get_aout_and_res_names (n_run), aout = t[0], res = t[1];
	var options = base @ " " @ opts[i];
	p.start (get_command (aout, res, options), [options, res, aout]);
	n_run++;
    }
  return n_run;
}

// Read and return lines of file RES.  Remove the file and AOUT.
// Return nil if something is wrong.
func process_result (res, aout) {
  var f, lns;
  try { f = open (res, "r"); } catch (invcalls.syserror) { return nil; }
  try { lns = fgetf (f, 1); } catch (invcalls.eof) {lns = nil;}
  close (f); try { remove (res); } catch (invcalls.syserror) {}
  try { remove (aout); } catch (invcalls.syserror) {}
  return lns;
}

// Print features line for function with full name EL compiled with
// flags OPTS and having execution time FACTOR.  The features are
// extracted from LNS.
func print_features (el, opts, lns, factor) {
  var v = split (el, "::"), fn = v[1];
  var features;
  for (var i = 0; i < #lns; i++)
    // Function name can have prefix _ for some languages.
    if (match ("^[*_A-Za-z]+::[_]?" @ fn, lns[i]) != nil) {
      // Remove features names
      features = gsub ("[-a-z0-9]+= ", lns[i], "");
      // Remove pass::function name
      // features = gsub ("[_A-Za-z]+::[_]?" @ fn, lns[i], "");
      putln ("+ \"", opts, "\" ", el, " ", factor, " ", features);
      break;
    }
}
// Return number of the line conatining compilation time, program and
// program, constrol sum
func find_run_features_start (dir, lns) {
  for (var i = 0; i < #lns; i++)
    if (match ("^[-+.0-9e]+ " @ dir @ " [0-9a-fA-F]*$", lns[i]) != nil)
      return i;
  return -1;
}

// The object represents a run of function with full NAME compiled
// with FLAGS needing CYCLES for its execution and BASE_CYLCES for its
// execution when compiled with the base flags.
class res_el (name, flags, cycles, base_cycles) {}

func main () { // Main script entry
  var n_all_dups = 0, n_all_tries = 0;
  for (var n = 0; n < #dirs; n++) {
    var n_run = 0, n_dups = 0, dir = dirs[n], lns;
    var big_degrades = {}, exclude = {};
    putln ("Processing ", dir);
    curr_dir = dir;
    lns = collect (dir, base); // Try just base options.
    if (#lns == 0) {
      putln ("Empty collect file!"); clean (); continue;
    }
    var best = {}, cs = {}, fs = split (lns[0]);
    // The first line format of COLLECT:
    // <compile time> <function name> <control sum>
    if (#fs != 3) {putln ("wrong collect output format"); exit (1);}
    cs {fs[2]} = 1; // Mark control sum
    for (i = 1; i < #lns; i++) { // Process function run-time characteristics
      // Format: <full function name> <exec cycles> other run-time characteristics.
      fs = split (lns[i]);
      if (fs[1] > 500) // Consider only functions with enough cycles samples
	best {fs[0]} = res_el (fs[0], base, fs[1] + 0, fs[1] + 0);
    }
    var fn_num = #best;
    if (fn_num == 0) { putln ("No enough sample functions!"); clean (); continue; }
    var n_tries = 0;
    // put runs with additional flags to run queue
    n_run = add_to_run (base, exclude, n_run);
    for (var cont_p = 1; cont_p;) {
      var improved = {};
      for (;; ) {
	var ready = p.get_ready ();
	if (ready == nil)
	  break; // No more runs
	n_tries++; n_all_tries++;
	var ok_p = ready[0];
	// Data is a triple [flags, result-file-name, exec-file-name]
	var data = ready[1], flags = data[0];
	var lns = process_result (data[1], data[2]);
	if (! ok_p) {
	  putln ("Compilation with ", flags, " failed"); continue;
	}
	if (lns == nil) {
	  putln ("Bad or absent output file ", data[1]); continue;
	}
	if (n_tries % 50 == 0) // Write a progress:
	  putln (n_tries, " option sets tried, to run = ", p.unfinished_num ());
	var features = new lns;
	var start = find_run_features_start (dir, lns);
	if (start < 0) {
	  putln ("Can not find run log start for ", dir); continue;
	}
	// Split output lines on features and run-time characteristics
	del (features, start, -1); del (lns, 0, start);
	fs = split (lns[0]);
	if (0 && fs[1] in cs) { // the same code
	  n_dups++; n_all_dups++;
	  var fa = split (flags), opt = fa[#fa -1];
	  exclude {opt} = 1;
	  continue;
	}
	if (#lns <= 1) putln ("No data for ", flags);
	for (i = 1; i < #lns; i++) {
	  fs = split (lns[i]);
	  var t = fs[1] + 0;
	  if (! (fs[0] in best))
	    // Something was wrong with function FS[0] or run with base
	    // flags or base run had not enough cycles.
	    continue;
	  var factor = t * 1. / best{fs[0]}.base_cycles;
	  putln ("Change ", fs[0], " ", t, " ", factor, " ", flags);
	  print_features (fs[0], flags, features, factor);
	  // Update best but only if improvement >= 2%
	  if (best{fs[0]}.cycles <= t
	      || (best{fs[0]}.base_cycles - t) * 100. / best{fs[0]}.base_cycles < 2.)
	    continue;
	  best {fs[0]} = res_el (fs[0], flags, fs[1] + 0, best {fs[0]}.base_cycles);
	  improved {fs[0]} = 1;
	}
      }
      cont_p = 0;
      if (0) { // Uncomment if you want to search several option
	       // chains.  It will take a lot of time.
	for (i in big_degrades) if (big_degrades{i} >= fn_num) exclude {i} = 1;
	  for (i in improved) n_run = add_to_run (best{i}[0], exclude, n_run);
	  cont_p = #improved;
      }
      putln ("Improved = ", #improved, " to run = ", p.unfinished_num (),
	     " excluded = ", #exclude);
    }
    var opts = {}; // Map flags -> best results with given felags
    for (i in best) // Form the map:
      if (best{i}.flags in opts) ins (opts{best{i}.flags}, best{i}, -1);
      else opts{best{i}.flags} = [best{i}];
    for (i in opts) { // Output best results:
      var obs = opts{i};
      for (var j = 0; j < #obs; j++) {
	putln ("Best for ", obs[j].name, " ", obs[j].flags, "(", obs[j].cycles, " vs ",
	       obs[j].base_cycles, " - ",
	       (obs[j].base_cycles - obs[j].cycles) * 100 / obs[j].base_cycles, "%)");
      }
    }
    putln ("Dups = ", n_dups);
    flush (stdout);
    clean ();
  }
  putln ("Overall tries = ", n_all_tries, ", Overall Dups = ", n_all_dups);
}

try {
  main ();
} catch (except) { // Finish if any unprocessed exception occurs
  clean ();
}
