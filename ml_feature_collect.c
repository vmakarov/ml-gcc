/* A common code used for two plugins used to get features for
   training and predictions.  */

#include "ml_plugin.h"

int plugin_is_GPL_compatible;
/* Names of GCC passes before the function optimizations, RA, and at
   the end of function processing.  */
const char *ml_opt_start_pass = "*all_optimizations";
const char *ml_ira_pass = "ira";
const char *ml_opt_end_pass = "*clean_state";

/* Function features about loops.  */
int ml_loops_num, ml_max_loop_depth, ml_leaf_loops_num;
/* Whole function and innermost loops features.  */
struct ml_features ml_function_features;
struct ml_features ml_loop_features;

/* Process tree EXPR recursively to update features FS.  Expr is
   currently in array reference of depth CURR_ARRAY_REF_DEPTH.  */
static void
process_expr (tree expr, struct ml_features *fs, int curr_array_ref_depth)
{
  int i, len;
  enum tree_code code;
  
  if (expr == NULL)
    return;

  switch (TREE_CODE (expr))
    {
    case MEM_REF:
      fs->mem_refs_num++;
      break;

    case ADDR_EXPR:
      code = TREE_CODE (TREE_OPERAND (expr, 0));
      if (code == PARM_DECL || code == VAR_DECL || code == COMPONENT_REF)
	fs->addr_refs_num++;
      break;

    case VAR_DECL:
      if (TREE_STATIC (expr))
	fs->static_refs_num++;
      break;

    case ARRAY_REF:
      fs->array_refs_num++;
      curr_array_ref_depth++;
      if (fs->max_array_refs_num < curr_array_ref_depth)
	fs->max_array_refs_num = curr_array_ref_depth;
      break;

    default:
      break;
    }

  len = TREE_OPERAND_LENGTH (expr);
  for (i = 0; i < len; i++)
    process_expr (TREE_OPERAND (expr, i), fs, curr_array_ref_depth);
  
}

/* Basic block currently being processed.  */
static basic_block curr_bb;
/* Calls number in the current BB and number of BBs with at least one
   call.  */
static int curr_bb_call_num, call_bbs_num;

/* Process STMT and its expressions to update features FS.  The
   function also updates BB_INSN_NUM.  */
static void
process_stmt (gimple stmt, struct ml_features *fs, int &bb_insn_num)
{
  int i, args_num;
  
  bb_insn_num++;
  fs->insns_num++;
  if (bb_insn_num > fs->max_bb_insns_num)
    fs->max_bb_insns_num = bb_insn_num;
  switch (gimple_code (stmt))
    {
    case GIMPLE_ASSIGN:
      process_expr (gimple_assign_lhs (stmt), fs, 0);
      process_expr (gimple_assign_rhs1 (stmt), fs, 0);
      if (gimple_assign_rhs_class (stmt) == GIMPLE_BINARY_RHS)
	{
	  tree tp = gimple_expr_type (stmt);
	  
	  process_expr (gimple_assign_rhs2 (stmt), fs, 0);
	  /* We are interesting in binary ops only.  */
	  if (TREE_CODE (tp) == INTEGER_TYPE)
	    fs->int_ops_num++;
	  else if (TREE_CODE (tp) == REAL_TYPE)
	    fs->float_ops_num++;
	}
      break;

    case GIMPLE_COND:
      {
	tree lhs = gimple_cond_lhs (stmt);
	tree_code code = TREE_CODE (TREE_TYPE (lhs));
	
	process_expr (lhs, fs, 0);
	process_expr (gimple_cond_rhs (stmt), fs, 0);
	if (code == INTEGER_TYPE)
	  fs->int_ops_num++;
	else if (code == REAL_TYPE)
	  fs->float_ops_num++;
	break;
      }
      
    case GIMPLE_CALL:
      fs->calls_num++;
      fs->calls_freq += curr_bb->frequency;
      curr_bb_call_num++;
      if (fs->max_bb_calls_num < curr_bb_call_num)
	fs->max_bb_calls_num = curr_bb_call_num;
      process_expr (gimple_call_lhs (stmt), fs, 0);
      process_expr (gimple_call_fn (stmt), fs, 0);
      args_num = gimple_call_num_args (stmt);
      for (i = 0; i < args_num; i++)
	{
	  tree arg = gimple_call_arg (stmt, i);
	  
	  process_expr (arg, fs, 0);
	}
      
      fs->call_args_num += args_num;
      if (fs->max_args_num < args_num)
	fs->max_args_num = args_num;
      break;
      
    default:
      for (i = 0; i < gimple_num_ops (stmt); i++)
	if (gimple_op (stmt, i) != NULL_TREE)  
	  process_expr (gimple_op (stmt, i), fs, 0);
    }
}

/* Process the current BB to update features FS on gimple level.  */
static void
process_curr_bb (struct ml_features *fs)
{
  edge e;
  edge_iterator ei;
  gimple phi;
  gimple_stmt_iterator gsi;
  int n, bb_insns_num;

  curr_bb_call_num = 0;
  fs->bbs_num++;
  fs->edges_num += EDGE_COUNT (curr_bb->preds);
  if (EDGE_COUNT (curr_bb->preds) > fs->max_bb_in_edges_num)
    fs->max_bb_in_edges_num = EDGE_COUNT (curr_bb->preds);
  if (EDGE_COUNT (curr_bb->succs) > fs->max_bb_out_edges_num)
    fs->max_bb_out_edges_num = EDGE_COUNT (curr_bb->succs);
  FOR_EACH_EDGE (e, ei, curr_bb->preds)
    {
      if (EDGE_CRITICAL_P (e))
	fs->critical_edges_num++;
      if (e->flags & EDGE_COMPLEX)
	fs->complex_edges_num++;
    }
  n = 0;
  for (gsi = gsi_start_phis (curr_bb); ! gsi_end_p (gsi); gsi_next (&gsi))
    {
      n++;
      phi = gsi_stmt (gsi);
      if (gimple_code (phi) != GIMPLE_PHI)
	break;
      int args_num = gimple_phi_num_args (phi);
      fs->phi_args_num += args_num;
      if (args_num > fs->max_phi_args_num)
	fs->max_phi_args_num = args_num;
    }
  bb_insns_num = 0;
  for (gsi = gsi_start_bb (curr_bb); !gsi_end_p (gsi); gsi_next (&gsi))
    process_stmt (gsi_stmt (gsi), fs, bb_insns_num);
  fs->phis_num += n;
  if (n > fs->max_phis_num)
    fs->max_phis_num = n;
  if (n * curr_bb->frequency > fs->max_phis_freq_num)
    fs->max_phis_freq_num = n * curr_bb->frequency;
  if (curr_bb_call_num > 0)
    call_bbs_num++;
}

/* Set up features on gimple level.  */
void
ml_collect_features (void)
{
  struct loop *loop;
  bool *innermost_loop_p;
  
  memset (&ml_function_features, 0, sizeof (ml_function_features));
  memset (&ml_loop_features, 0, sizeof (ml_loop_features));
  ml_loops_num = number_of_loops (cfun) - 1;
  ml_max_loop_depth = 0;
  ml_leaf_loops_num = 0;
  innermost_loop_p = XNEWVEC (bool, ml_loops_num + 1);
  memset (innermost_loop_p, 0, sizeof (bool) * ml_loops_num + 1);
  FOR_EACH_LOOP (loop, LI_ONLY_INNERMOST)
    {
      innermost_loop_p[loop->num] = true;
      ml_leaf_loops_num++;
      if (loop_depth (loop) > ml_max_loop_depth)
	ml_max_loop_depth = loop_depth (loop);
    }
  call_bbs_num = 0;
  FOR_EACH_BB_FN (curr_bb, cfun)
    process_curr_bb (&ml_function_features);
  if (call_bbs_num != 0) {
    ml_function_features.average_bb_calls_num
      = ml_function_features.calls_num * 10 / call_bbs_num;
    ml_function_features.average_bb_calls_freq
      = ml_function_features.calls_freq * 10 / call_bbs_num;
  }
  call_bbs_num = 0;
  FOR_EACH_BB_FN (curr_bb, cfun)
    if (innermost_loop_p [curr_bb->loop_father->num])
      process_curr_bb (&ml_loop_features);
  if (call_bbs_num != 0) {
    ml_loop_features.average_bb_calls_num
      = ml_loop_features.calls_num * 10 / call_bbs_num;
    ml_loop_features.average_bb_calls_freq
      = ml_loop_features.calls_freq * 10 / call_bbs_num;
  }
  free (innermost_loop_p);
}

/* Process current BB to update features FS on RTL level.  */
static void
process_curr_rtl_bb (struct ml_features *fs)
{
  bitmap_iterator bi;
  unsigned int r;
  int in = 0, fn = 0;
  
  EXECUTE_IF_SET_IN_BITMAP (DF_LR_IN (curr_bb), 0, r, bi)
    {
      if (INTEGRAL_MODE_P (GET_MODE (regno_reg_rtx [r])))
	in++;
      else if (FLOAT_MODE_P (GET_MODE (regno_reg_rtx [r])))
	fn++;
    }
  if (fs->max_int_pressure < in)
    fs->max_int_pressure = in;
  if (fs->max_float_pressure < fn)
    fs->max_float_pressure = fn;
}

/* Set up features on RTL level.  */
void
ml_collect_rtl_features (void)
{
  struct loop *loop;
  bool *innermost_loop_p;
  int loops_num;
  loop_optimizer_init (AVOID_CFG_MODIFICATIONS);
  loops_num = number_of_loops (cfun) - 1;
 
  innermost_loop_p = XNEWVEC (bool, loops_num + 1);
  memset (innermost_loop_p, 0, sizeof (bool) * loops_num + 1);
  FOR_EACH_LOOP (loop, LI_ONLY_INNERMOST)
    innermost_loop_p[loop->num] = true;
  FOR_EACH_BB_FN (curr_bb, cfun)
    process_curr_rtl_bb (&ml_function_features);
  FOR_EACH_BB_FN (curr_bb, cfun)
    if (innermost_loop_p [curr_bb->loop_father->num])
      process_curr_rtl_bb (&ml_loop_features);
  loop_optimizer_finalize ();
  free (innermost_loop_p);
}
