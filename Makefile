HOST_GCC=g++
TARGET_GCC=gcc
COMMON_PLUGIN_SOURCE_FILES= ml_feature_collect.c
GCCPLUGINS_DIR:= $(shell $(TARGET_GCC) -print-file-name=plugin)
CXXFLAGS+= -I$(GCCPLUGINS_DIR)/include -fPIC -O2 -g
  
all: ml_features.so ml_options.so ml_check_svm

ml_features.so: ml_features.c $(COMMON_PLUGIN_SOURCE_FILES) ml_data_read.c
	$(HOST_GCC) -shared $(CXXFLAGS) ml_features.c $(COMMON_PLUGIN_SOURCE_FILES) -o $@
ml_options.so: ml_options.c $(COMMON_PLUGIN_SOURCE_FILES) ml_data_read.c
	$(HOST_GCC) -shared $(CXXFLAGS) ml_options.c $(COMMON_PLUGIN_SOURCE_FILES) -o $@ -lsvm -lm
ml_check_svm: ml_check_svm.c ml_data_read.c
	$(HOST_GCC) -O2 -g ml_check_svm.c -o $@ -lsvm -lm
clean:
	rm -rf ml_features.so ml_options.so ml_check_svm
