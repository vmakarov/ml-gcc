// Script for transforming our training/test data (see comments for
// ml_data_read.c) files into files in format of svm-scale and
// svm-train.
//
// Usage: <program> <data file>
//
// Standard output: input file for svm-train and svm-scale.


if (#argv != 1) { fputln (stderr, "Usage: <data file>"); exit (1); }
var f = open (argv[0], "r");
var rows_num = fscan (f);
var fs_num = fscan (f);
var opts_num = fscanln (f);

// Skip options:
for (var i = 0; i < opts_num; i++) fgetln (f);

for (i = 0; i < rows_num; i++) {
  var ln = fgetln (f);
  var fs = split (ln);
  if (#fs != fs_num + opts_num) {
     fputln (stderr, "wrong line fields number"); exit (1);
  }
  // Choose best options as expected output
  var n, best = 100.;
  for (var j = 0; j < opts_num; j++)
    if (best > fs[j]) { best = fs[j]; n = j + 1;}
  // Use the default class (base options) if we have less 3%
  // improvement:
  if (best >= 0.97) n = 0;
  // Print line in format fo svm-train
  put (n); // expected output
  for (j = 0; j < fs_num; j++)
    // <feature number> : <feature value>
    put (" ", j + 1, ":", fs[j + opts_num]);
  putln ();
}
close (f);
