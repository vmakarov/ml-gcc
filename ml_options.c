/* Plugin predicting options based on SVM. The plugin reads train data
   file <name>, scale file <name>.scales, and SVM model file
   <name>.model.  Where <name> is a value of plugin argument "name"
   (use GCC option -fplugin-arg-ml_options-name=<name> for this).  The
   plugin chooses what option set to use for each function.  */
#include <libsvm/svm.h>
#include "ml_plugin.h"
#include "ml_data_read.c"
#include "params.h"

/* Number of features we are collecting.  Input data should have the
   same number of features.  */
static const int features_num = 57;

/* Add features FS to feature vector FV starting with S element.
   Return last element of FV used for adding.  */
static int
add_features (float *fv, struct ml_features *fs, int s)
{
  fv[s + 0] = fs->bbs_num;
  fv[s + 1] = fs->edges_num;
  fv[s + 2] = fs->critical_edges_num;
  fv[s + 3] = fs->complex_edges_num;
  fv[s + 4] = fs->max_bb_in_edges_num;
  fv[s + 5] = fs->max_bb_out_edges_num;
  fv[s + 6] = fs->phis_num;
  fv[s + 7] = fs->max_phis_num;
  fv[s + 8] = fs->phi_args_num;
  fv[s + 9] = fs->max_phi_args_num;
  fv[s + 10] = fs->max_phis_freq_num;
  fv[s + 11] = fs->insns_num;
  fv[s + 12] = fs->max_bb_insns_num;
  fv[s + 13] = fs->calls_num;
  fv[s + 14] = fs->calls_freq;
  fv[s + 15] = fs->call_args_num;
  fv[s + 16] = fs->max_args_num;
  fv[s + 17] = fs->max_bb_calls_num;
  fv[s + 18] = fs->average_bb_calls_num;
  fv[s + 19] = fs->average_bb_calls_freq;
  fv[s + 20] = fs->int_ops_num;
  fv[s + 21] = fs->float_ops_num;
  fv[s + 22] = fs->mem_refs_num;
  fv[s + 23] = fs->addr_refs_num;
  fv[s + 24] = fs->static_refs_num;
  fv[s + 25] = fs->array_refs_num;
  fv[s + 26] = fs->max_array_refs_num;
  return s + 26;
}

/* SVM model used.  */
static struct svm_model *ml_model;
/* SVM test vector used for prediction.  */
static struct svm_node *ml_test;
/* Number of options in the train data file. */
static int ml_opts_num;
/* The opts itself.  */
static char **ml_opts;

/* Allocate and set up and scale feature vector.  The element order
   should be the same as in printing features in ml-feature
   plugin.  */
static float *
create_feature_vector (void)
{
  float *fv;
  int last, i;
  
  fv = XNEWVEC (float, features_num);
  fv[0] = ml_loops_num; fv[1] = ml_max_loop_depth; fv[2] = ml_leaf_loops_num;
  last = add_features (fv, &ml_function_features, 3);
  last = add_features (fv, &ml_loop_features, last + 1);
  assert (last + 1 == features_num);
  scale_input (fv, features_num);
  return fv;
}

/* Element describing options we can set up.  */
struct ml_opt_desc
{
  const char *name; /* option as a string  */
   /* GCC flag which this option changes.  If it is NULL we do nothing
      if the option occurs.  */
  int *flag;
   /* The flag value should be used when the option occurs.  */
  int val;
  int default_val; /* The default value of the flag.  */
};

/* Array describing options the plugin can use.  At least all options
   mentioned in the train file should be in the table.  */
static struct ml_opt_desc ml_options[] = {
  {"-O3", NULL, 0},
  {"-w", NULL, 0},
  {"-fmodulo-sched", &flag_modulo_sched, 1},
  {"-fmodulo-sched-allow-regmoves", &flag_modulo_sched_allow_regmoves, 1},
  {"-fno-sched-group-heuristic", &flag_sched_group_heuristic, 0},
  {"-fno-sched-spec-insn-heuristic", &flag_sched_spec_insn_heuristic, 0},
  {"-fno-sched-rank-heuristic", &flag_sched_rank_heuristic, 0},
  {"-fno-sched-last-insn-heuristic", &flag_sched_last_insn_heuristic, 0},
  {"-fno-sched-dep-count-heuristic", &flag_sched_dep_count_heuristic, 0},
  {"-fsched-pressure", &flag_sched_pressure, 1},
  {"-fsched2-use-traces", NULL, 0},
  {"-fsched-spec-load", &flag_schedule_speculative_load, 1},
  {"-fsel-sched-pipelining", &flag_sel_sched_pipelining, 1},
  {"-fconserve-stack", &flag_conserve_stack, 1},
  {"-fgraphite-identity", &flag_graphite_identity, 1},
  {"-fira-region=one", (int*) &flag_ira_region, IRA_REGION_ONE},
  {"-fno-if-conversion2", &flag_if_conversion2, 0},
  {"-fno-ipa-pure-const", &flag_ipa_pure_const, 0},
  {"-fno-ivopts", &flag_ivopts, 0},
  {"-fno-partial-inlining", &flag_partial_inlining, 0},
  {"-fno-peephole2", &flag_peephole2, 0},
  {"-fno-predictive-commoning", &flag_predictive_commoning, 0},
  {"-fno-reorder-functions", &flag_reorder_functions, 0},
  {"-fno-rerun-cse-after-loop", &flag_rerun_cse_after_loop, 0},
  {"-fno-sched-dep-count-heuristic", &flag_sched_dep_count_heuristic, 0},
  {"-fno-strict-aliasing", &flag_strict_aliasing, 0},
  {"-fno-toplevel-reorder", &flag_toplevel_reorder, 0},
  {"-fno-tree-loop-im", &flag_tree_loop_im, 0},
  {"-fno-tree-partial-pre", &flag_tree_partial_pre, 0},
  {"-fno-tree-pre", &flag_tree_pre, 0},
  {"-fno-tree-reassoc", &flag_tree_reassoc, 0},
  {"-fno-tree-vectorize", &flag_tree_vectorize, 0},
  {"-fsched2-use-superblocks", &flag_sched2_use_superblocks, 1},
  {"-fsched-stalled-insns", &flag_sched_stalled_insns, 1},
  {"-fschedule-insns", &flag_schedule_insns, 1},
  {"-fselective-scheduling", &flag_selective_scheduling, 1},
  {"-fselective-scheduling2", &flag_selective_scheduling2, 1},
  {"-ftracer", &flag_tracer, 1},
  {"-ftree-loop-linear", &flag_loop_interchange, 1},
  {"-fipa-pta", &flag_ipa_pta, 1},
  {"-fno-dce", &flag_dce, 0},
  {"-fno-expensive-optimizations", &flag_expensive_optimizations, 0},
  {"-fno-ipa-cp", &flag_ipa_cp, 0},
  {"-fno-optimize-sibling-calls", &flag_optimize_sibling_calls, 0},
  {"-fno-shrink-wrap", &flag_shrink_wrap, 0},
  {"-fno-tree-dce", &flag_tree_dce, 0},
  {"-fno-tree-forwprop", &flag_tree_forwprop, 0},
  {"-fno-tree-loop-if-convert", &flag_tree_loop_if_convert, 0},
  {"-fno-tree-loop-vectorize", &flag_tree_loop_vectorize, 0},
  {"-fno-tree-sink", &flag_tree_sink, 0},
  {"-fno-cprop-registers", &flag_cprop_registers, 0},
  {"-fno-gcse", &flag_gcse, 0},
  {"-fno-tree-copy-prop", &flag_tree_copy_prop, 0},
  {"-fno-tree-fre", &flag_tree_fre, 0},
  {"-fno-tree-loop-optimize", &flag_tree_loop_optimize, 0},
  {"-fvect-cost-model=cheap", (int*) &flag_vect_cost_model, VECT_COST_MODEL_CHEAP},
  {"-funroll-loops", &flag_unroll_loops, 1},
  {"-funroll-all-loops", &flag_unroll_all_loops, 1},
  {"-fno-guess-branch-probability", &flag_guess_branch_prob, 0},
  {"-fno-tree-copyrename", &flag_tree_copyrename, 0},
  {"-fno-omit-frame-pointer", &flag_omit_frame_pointer, 0},
  {"-fno-caller-saves", &flag_caller_saves, 0},
  {"-fno-if-conversion", &flag_if_conversion, 0},
  {"-fno-align-loops", &align_loops, 0}, // ???
  {"-fno-tree-dominator-opts", &flag_tree_dom, 0},
  {"-fno-schedule-insns2", &flag_schedule_insns_after_reload, 0},
  {"-fno-cse-follow-jumps", &flag_cse_follow_jumps, 0},
  {"-fno-crossjumping", &flag_crossjumping, 0},
  {"-fno-align-jumps", &align_jumps, 0},
  {"-fprefetch-loop-arrays", &flag_prefetch_loop_arrays, 1},
  {"-fno-align-functions", &align_functions, 0},
  {"-ftree-loop-distribution", &flag_tree_loop_distribution, 1},
  {"max-unrolled-insns=10",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 10},
  {"max-unrolled-insns=20",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 20},
  {"max-unrolled-insns=40",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 40},
  {"max-unrolled-insns=80",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 80},
  {"max-unrolled-insns=160",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 160},
  {"max-unrolled-insns=320",
   &global_options.x_param_values[PARAM_MAX_UNROLLED_INSNS], 320},
  {"prefetch-latency=10",
   &global_options.x_param_values[PARAM_PREFETCH_LATENCY], 10},
  {"prefetch-latency=40",
   &global_options.x_param_values[PARAM_PREFETCH_LATENCY], 40},
  {"prefetch-latency=200",
   &global_options.x_param_values[PARAM_PREFETCH_LATENCY], 200},
};

/* Given set of options OPTS, set the corresponding flag values if
   SET_P, or rest them to their default values otherwise.  */
static void
set_opts (const char *opts, bool set_p)
{
  char str[1000];
  int i, n;
  for (;;)
    {
      if (*opts == 0 || sscanf (opts, "%s ", str) == 0)
	break;
      while (*opts != str[0])
	opts++;
      opts += strlen (str);
      if (strcmp (str, "--param") == 0)
	{
	  if (sscanf (opts, "%s ", str) == 0) {
	    fprintf (stderr, "Absent param after --param\n");
	    exit(1);
	  }
	  while (*opts != str[0])
	    opts++;
	  opts += strlen (str);
	}
      n = sizeof (ml_options) / sizeof (ml_options[0]);
      for (i = 0; i < n; i++)
	if (strcmp (ml_options[i].name, str) == 0)
	  {
	    if (ml_options[i].flag == NULL)
	      break;
	    if (! set_p)
	      *ml_options[i].flag = ml_options[i].default_val;
	    else
	      *ml_options[i].flag = ml_options[i].val;
	    break;
	  }
      if (i >= n)
	{
	  fprintf (stderr, "Can not find decription of option %s\n", str);
	  exit(1);
	}
    }
}

/* Options set used for the current function is stored in the
   following array.  */
static char ml_last_opts[1000];

static void
pass_execution_callback (void *gcc_data, void *user_data)
{
  const opt_pass *p = (opt_pass *) gcc_data;
  float *fv, *calc_out;
  int i, result;

  if (p->name == NULL || cfun == NULL)
    return;
  if (strcmp (p->name, ml_opt_start_pass) == 0)
    {
      //      fprintf (stderr, "Collecting features for ");
      ml_collect_features ();
      fv = create_feature_vector ();
      for (i = 0; i < features_num; i++) {
	ml_test[i].index = i + 1;
	ml_test[i].value = fv[i];//, printf (" %g", ml_test[i].value);
      }
      ml_test[i].index = -1;
      int result = svm_predict (ml_model, ml_test) - 1;
      if (result != -1)
	strcpy (ml_last_opts, ml_opts[result]);
      else
	strcpy (ml_last_opts, "-O3 -w");
      fprintf
	(stderr, "%s::%s:ind=%d,opts=%s\n", p->name,
	 identifier_to_locale (IDENTIFIER_POINTER (DECL_NAME (cfun->decl))),
	 result, ml_last_opts);
      set_opts (ml_last_opts, true);
      free (fv);
    }
  else if (strcmp (p->name, ml_ira_pass) == 0)
    {
    }
  else if (strcmp (p->name, ml_opt_end_pass) == 0)
    {
      fprintf (stderr, "%s\n", ml_last_opts);
      set_opts (ml_last_opts, false);
    }
}

/* Return concatenation of FILENAME and EXT.  The previous result
   dissapers.  */
static char *
get_fullname (const char *filename, const char *ext)
{
#define MAX_FNAME 1000
  static char fullname[MAX_FNAME + 1];
  
  if (strlen (filename) + strlen (ext) >= MAX_FNAME)
    {
      fprintf (stderr, "Too long file name %s with extension %s\n", filename, ext);
      exit(1);
    }
  strcpy (fullname, filename);
  strcpy (fullname + strlen (fullname), ext);
  return fullname;
}

/* The plugin argument name.  */
static const char *plugin_arg_name = "name";

/* Return the plugin argument value, or NULL if the argument is not
   given.  */ 
static const char *
get_arg_value (struct plugin_name_args *plugin_info)
{
  int i;
  
  for (i = 0; i < plugin_info->argc; i++)
    if (strcmp (plugin_info->argv[i].key, plugin_arg_name) == 0)
      return plugin_info->argv[i].value;
  return NULL;
}

/* Finish the plugin: free allocated memory.  */
static void
finish_callback (void *gcc_data, void *user_data)
{
  int i;
  
  for (i = 0; i < ml_opts_num; i++)
    free (ml_opts[i]);
  free (ml_opts);
  free (ml_test);
  svm_free_model_content (ml_model);
  free (rs);
}

/* Plugin inititialization function.  It checks and read the necessary
   files, exit if something is wrong.  */
int
plugin_init (struct plugin_name_args *plugin_info,
	     struct plugin_gcc_version *version)
{
  const char *fn;
  int i, row, inp;
  FILE *f;
  float *input;
  float *output;
#if VERSION_CHECK
  if (!plugin_default_version_check (version, &gcc_version))
    return 1;
#endif

  register_callback ("pass execution callback", PLUGIN_PASS_EXECUTION,
		     pass_execution_callback, NULL);
  register_callback ("pass execution callback", PLUGIN_FINISH,
		     finish_callback, NULL);
  fn = get_arg_value (plugin_info);
  if (fn == NULL)
    {
      fprintf (stderr, "Plugin arg '%s' was not given\n", plugin_arg_name);
      exit (1);
    }
  f = get_file (fn, "r");
  read_file (f, &input, &output, &ml_opts, &row, &inp, &ml_opts_num);
  if (inp != features_num) {
    fprintf (stderr, "Worng expected features number in the train file\n");
    exit (1);
  }
  if (inp < read_scales (get_fullname (fn, ".scales"))) {
    fprintf (stderr, "Bigger features number in the scale file\n");
    exit (1);
  }
  fprintf (stderr, "%d,%d\n", inp, features_num);
  free (input);
  free (output); // We don't need input/output of the train file for prediction
  for (i = 0; i < sizeof (ml_options) / sizeof (ml_options[0]); i++)
    if (ml_options[i].flag != NULL)
      ml_options[i].default_val = *ml_options[i].flag;
  ml_model = svm_load_model (get_fullname (fn, ".model"));
  ml_test = (struct svm_node *) get_mem ((features_num + 1)
					 * sizeof (struct svm_node));
  return 0;
}
