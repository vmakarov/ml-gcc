  This is a research project to investigate using Machine Learning in
choosing GCC options.

  To get training/testing data, we use benchmarks from SPEC, Aburto,
computer languages shootout and other sources.  The major criteria to
choose benchmarks is time to compile and run time.  As we want to get
data for numerous different options, we constrain compile and run-time
to a few seconds.  Even with such strict constraints, getting data can
take several hours even on fast machines.

  To add a new program, we should create a new directory and write 2
scripts compile and run.

  Script 'compile' first argument is a.out file and rest arguments
compile option.  The script should use environemnt variables CC, CXX,
and FC as used compiler for C C++ and Fortran files.

  Script 'run' arguments are the compiled program and its arguments.
After the program run the script should call command given by
environment variable REPORT.

  To get training/testing data:

  1. Run 'dino try.d * | tee LOG' in directory containing directories
     with the benchmarks.  This is the single long run.  It will take
     several hours.
  2. Extract data from the log file 'dino log2data.d LOG >D'.
  3. Split the data file 'dino split-data.d D'
  4. Transform our data train file into a file in svm format:
     'dino data2svm.d D.train >D.train.svm'
  5. Scale the training data:
        'svm-scale -s D.train.svm.scales D.train.svm >D.train.svm.scaled'.
     Scaling training/test data is very important to get better results.
  6. Transform svm scale file into our data scale file
        'dino svm2data-scale.d D.train.svm.scales > D.train.scales'
  7. Find the best params of SVM:
     for i in 0.125 0.25 0.5 1.25 1.5 2.0 4.0 8.0;do
       for j in 1 2 3 4 8 16;do
          echo $i $j
	  svm-train -c $j -g $i -q D.train.svm.scaled
	  ./ml_check_svm D.train.svm.scaled.model D.train.scales D.test >/dev/null
       done
     done
  8. Prepare files for the prediction plugin:
       cp D.train.svm.scaled.model D.train.model
  9. Use the prediction plugin:
       <target-gcc> <base> -fplugin=<path-to>/ml_options.c
                           -fplugin-arg-ml_options-name=<path-to>/D.train
