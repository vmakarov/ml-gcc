// Usage: program <svm input file>.
// The input file has svm-train format.  Divide the input file onto
// train (suffix .train) and test (suffix .test) files.

if (#argv != 1) {putln ("Usage: program <input svm file>"); exit (1);}
var f = open (argv[0], "r"), lns = fgetf (f, 1); close (f);

func class_cmp (l1, l2) { // used to sort by expected class
  l1 = split (l1); l2 = split (l2); return l1[0] - l2[0];
}

lns = sort (lns, class_cmp);

// Return max class (it is the first field) in LNS
func get_max_class (lns) {
  var max_class = -1;
  for (var i = 0; i < #lns; i++) {
    var c = split (lns[i])[0] + 0;
    if (max_class < c) max_class = c;
  }
  return max_class;
}

func form_class_map (lns) { // Form map: class -> array of lns starting with the class
  var max_class = get_max_class (lns), map = [max_class + 1 : nil];
  for (var i = 0; i < #lns; i++) { // form map
    var c = split (lns[i])[0] + 0;
    if (map[c] == nil) map[c] = [lns[i]];
    else ins (map[c], lns[i], -1);
  }
  return map;
}

var map = form_class_map (lns);

var train = open (argv[0] @ ".train", "w"), test = open (argv[0] @ ".test", "w");

for (var i = 0; i < #map; i++) { // Form the training/test files
  if (map[i] == nil) continue;
  var to_train = (#map[i] + 1) / 2;
  for (var j = 0; j < to_train; j++)
    fputln (train, map[i][j]);
  for (j = to_train; j < #map[i]; j++)
    fputln (test, map[i][j]);
}
close (train); close (test);
