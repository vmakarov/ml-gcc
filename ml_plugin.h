/* The header file for common code used to collect features for
   training and prediction plugins.  It is oriented to gcc-4.9 and
   gcc-5.0.  Previous versions have other header structure.  */
#include <string.h>
#include <assert.h>
#include "gcc-plugin.h"
#include "plugin-version.h"

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tree.h"
#include "intl.h"
#include "tm.h"
#include "diagnostic.h"
#include "tree-pass.h"
#include "function.h"
#include "basic-block.h"
#include "cfgloop.h"
#include "tree-ssa-alias.h"
#include "gimple-expr.h"
#include "is-a.h"
#include "gimple.h"
#include "gimple-iterator.h"
#include "gimple-ssa.h"
#include "print-tree.h"
#include "rtl.h"
#include "df.h"

extern int plugin_is_GPL_compatible;
/* Names of passes before the function optimizations, RA, and at the
   end of function processing.  */
extern const char *ml_opt_start_pass;
extern const char *ml_ira_pass;
extern const char *ml_opt_end_pass;

/* Function/loop features we are collecting  */
struct ml_features
{
  int bbs_num, edges_num, critical_edges_num, complex_edges_num;
  int max_bb_in_edges_num, max_bb_out_edges_num;
  int phis_num, max_phis_num, phi_args_num, max_phi_args_num, max_phis_freq_num;
  int insns_num, max_bb_insns_num, calls_num, calls_freq, call_args_num, max_args_num;
  int max_bb_calls_num, average_bb_calls_num, average_bb_calls_freq;
  int int_ops_num, float_ops_num, mem_refs_num, addr_refs_num, static_refs_num;
  int array_refs_num, max_array_refs_num;
  int max_int_pressure, max_float_pressure;
};

/* Function features about loops.  */
extern int ml_loops_num, ml_max_loop_depth, ml_leaf_loops_num;
/* Whole function and innermost loops features.  */
extern struct ml_features ml_function_features;
extern struct ml_features ml_loop_features;

extern void ml_collect_features (void);
extern void ml_collect_rtl_features (void);
