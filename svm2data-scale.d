// Usage: <svm-scale-file>
// Standard output is the our data scale file.

if (#argv != 1) {
  fputln (stderr, "Usage: <svm-scale-file>");
  exit (1);
}

var f = open (argv[0], "r");
var lns = fgetf (f, 1);
close (f);

lns[1] = split (lns[1]);
if (#lns[1] != 2) {
  fputln (stderr, "wrong the first line of file ", argv[0]);
  exit (1);
}

var rmin = float (lns[1][0]), rmax = float (lns[1][1]);
var span = rmax - rmin;
if (span < 0) {
  fputln (stderr, "wrong the first line value range of file ", argv[0]);
  exit (1);
}

for (var i = 2; i < #lns; i++) { // check field ranges:
  var ln = split (lns[i]);
  if (#ln != 3 || ln[0] < i - 1) {
    fputln (stderr, "wrong line '", lns[i], "' of file ", argv[0]);
    exit (1);
  }
  if (ln[0] != i - 1) { // add feature skipped because it has the same value
    ins (lns, i - 1 @ " 0 0", i); i--; continue;
  }
  if (ln[1] > ln[2]) {
    fputln (stderr, "wrong line '", lns[i], "' of file ", argv[0]);
    exit (1);
  }
  lns[i] = ln;
}

// Output scale file:
var fs_num = #lns - 2;
putln (fs_num, " ", rmin, " ", rmax);
for (i = 0; i < fs_num; i++)
  putln (lns[i + 2][1], " ", lns[i + 2][2]);
