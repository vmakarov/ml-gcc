// Usage: program <input data file>.
// The input file has data format.  Divide the input file onto
// train (suffix .train) and test (suffix .test) data files.

if (#argv != 1) {putln ("Usage: program <input svm file>"); exit (1);}
var f = open (argv[0], "r");
var rows_num = fscan (f);
var fs_num = fscan (f);
var opts_num = fscanln (f);

if (rows_num < 3) {fputln ("too few data"); exit (1);}

var train_rows_num = rows_num / 2;
var train = open (argv[0] @ ".train", "w"), test = open (argv[0] @ ".test", "w");

fputln (train, train_rows_num, " ", fs_num, " ", opts_num);
fputln (test, rows_num - train_rows_num, " ", fs_num, " ", opts_num);

// Output options:
for (var i = 0; i < opts_num; i++) {
  var ln = fgetln (f);
  fputln (train, ln); fputln (test, ln);
}

// Output rows to the train file:
for (i = 0; i < train_rows_num; i++) {
  var ln = fgetln (f);
  fputln (train, ln);
}

// Output rows to the test file:
for (i = 0; i < rows_num - train_rows_num; i++) {
  var ln = fgetln (f);
  fputln (test, ln);
}

close (train); close (test);
