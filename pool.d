// The file supports queue to run shell processes.  We don't run
// simultaneously more processes than a given number.  On some system
// with multi-core processors, we can decrease expected wall time to
// get the results.  Of course to get an accurate results we need to
// run simultaneously no more processes than the processors number.

// Class for pool to run no more N shell processes
class pool (n) {
  var cpid = getpid ();
  // Prefixes of dirs used for synchronization.  They mark finishing
  // processes.
  var sync_ok_dir_prefix = "/tmp/done_" @ cpid @ "_";
  var sync_fail_dir_prefix = "/tmp/fail_" @ cpid @ "_";
  
  var queue = []; // Contains pairs (command, data) which should be started
  var slots = [n: nil]; // Contains info about run commands -- see slot_el
  private queue, slots, cpid, sync_ok_dir_prefix, sync_fail_dir_prefix;

  // COMMAND run with DATA using OK_DIR and FAIL_DIR as sync dirs.
  class slot_el (ok_dir, fail_dir, data, command) {}
  private slot_el;
  
  // Start commands from the queue as many as allowed
  func start_from_queue () {
    for (var i = 0; i < #slots; i++)
      if (slots[i] == nil) {
	if (#queue == 0) break;
	var el = queue[0]; del (queue, 0);
	//putln ("starting ", el[0]);
	var ok_dir = sync_ok_dir_prefix @ i;
	var fail_dir = sync_fail_dir_prefix @ i;
	system ("if " @ el[0] @ "; then mkdir " @ ok_dir
		@ "; else mkdir " @ fail_dir @ ";fi &");
	slots[i] = slot_el (ok_dir, fail_dir, el[1], el[0]);
      }
  }
  private start_from_queue;
  
  // Add COMMAND with DATA to queue of the process to run and start
  // commands from the queue as many as allowed
  func start (command, data) {
    ins (queue, [command, data], -1);
    start_from_queue ();
  }
  
  // Start commands from the queue as many as allowed.  Return pair
  // [status, data] of a finished process or nil if there are no
  // processes to run.
  func get_ready () {
    start_from_queue ();
    for (;;) {
      var cont_p = 0;
      for (var i = 0; i < #slots; i++)
	if (slots[i] != nil) {
	  var ok_dir = slots[i].ok_dir, fail_dir = slots[i].fail_dir, ok;
	  if (system ("rmdir " @ ok_dir @ " 2>/dev/null") == 0)
	    ok = 1;
	  else if (system ("rmdir " @ fail_dir @ " 2>/dev/null") == 0)
	    ok = 0;
	  else {
	    cont_p = 1; continue;
	  }
	  var data = slots[i].data;
	  slots[i] = nil; // Mark the slot is free
	  return [ok, data];
	}
      if (! cont_p)
	break;
      system ("sleep 0.3");
    }
    return nil;
  }
  
  func unfinished_num () { // Return # of processes to run or finish
    var res = 0;
    for (var i = 0; i < n; i++)
      if (slots[i] != nil)
	res++;
    return res + #queue;
  }
  
  func destroy () { // Clean up unprocessed runs.
    for (var i = 0; i < #slots; i++)
      if (slots[i] != nil) {
	system ("rmdir " @ slots[i].ok_dir @ " 2>/dev/null || rmdir "
		@ slots[i].fail_dir @ " 2>/dev/null;");
	slots[i] = nil;
      }
  }
}
