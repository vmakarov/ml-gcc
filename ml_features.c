/* This is a plugin file to get program features for the subequent
   training.  The output format:

   <pass>::<function_name> <feature1_name>= value <feature2_name>= value ...
*/
#include "ml_plugin.h"

/* Print gimple-level features from FS, each feature name will start
   with prefix P.  */
static void
print_features (struct ml_features *fs, const char *p)
{
  printf ("%sbbs= %d %sedges= %d %scritical-edges= %d %scomplex-edges= %d ",
	  p, fs->bbs_num, p, fs->edges_num, p, fs->critical_edges_num,
	  p, fs->complex_edges_num);
  printf ("%smax-bb-in-edges= %d %smax-bb-out-edges= %d ",
	  p, fs->max_bb_in_edges_num, p, fs->max_bb_out_edges_num);
  printf ("%sphis= %d %smax-phis= %d %sphi-args= %d %smax-phi-args= %d ",
	  p, fs->phis_num, p, fs->max_phis_num, p, fs->phi_args_num,
	  p, fs->max_phi_args_num);
  printf ("%smax-phis-freq= %d %sinsns= %d %smax-bb-insns= %d ",
	  p, fs->max_phis_freq_num, p, fs->insns_num, p, fs->max_bb_insns_num);
  printf ("%scalls-num= %d %scalls-freq= %d ",
	  p, fs->calls_num, p, fs->calls_freq);
  printf ("%scall-args-num= %d %smax-args-num= %d ",
	  p, fs->call_args_num, p, fs->max_args_num);
  printf ("%smax-bb-calls-num= %d %saverage-bb-calls-num= %d ",
	  p, fs->max_bb_calls_num, p, fs->average_bb_calls_num);
  printf ("%saverage-bb-calls-freq= %d ",
	  p, fs->average_bb_calls_freq);
  printf ("%sint-ops= %d %sfloat-ops= %d %sind-refs= %d ",
	  p, fs->int_ops_num, p, fs->float_ops_num, p, fs->mem_refs_num);
  printf ("%saddr-refs= %d %sstatic-refs= %d ",
	  p, fs->addr_refs_num, p, fs->static_refs_num);
  printf ("%sarray-refs= %d %smax-array-refs= %d ",
	  p, fs->array_refs_num, p, fs->max_array_refs_num);
}

/* Print rtl-level features from FS, each feature name will start with
   prefix P.  */
static void
print_rtl_features (struct ml_features *fs, const char *p)
{
  printf ("%smax-int-pressure= %d %smax-float-pressure= %d ",
	  p, fs->max_int_pressure, p, fs->max_float_pressure);
}

/* Plugin call-back called at the each pass start. */
static void
pass_execution_callback (void *gcc_data, void *user_data)
{
  const opt_pass *p = (opt_pass *) gcc_data;
  
  if (p->name == NULL || cfun == NULL)
    return;
  if (strcmp (p->name, ml_opt_start_pass) == 0)
    {
      /* Collect features on rtl level.  */
      printf ("%s::%s ", p->name,
	      identifier_to_locale (IDENTIFIER_POINTER (DECL_NAME (cfun->decl))));
      ml_collect_features ();
      printf ("loops= %d max-depth= %d leaf-loops= %d ",
	      ml_loops_num, ml_max_loop_depth, ml_leaf_loops_num);
      print_features (&ml_function_features, "");
      print_features (&ml_loop_features, "loop-");
    }
  else if (strcmp (p->name, ml_ira_pass) == 0)
    {
      //ml_collect_rtl_features ();
      //print_rtl_features (&ml_function_features, "");
      //print_rtl_features (&ml_loop_features, "loop-");
    }
  else if (strcmp (p->name, ml_opt_end_pass) == 0)
    printf ("\n");
}

/* Plugin inititialization function to set up the above call-back for
   each pass.  */
int
plugin_init (struct plugin_name_args *plugin_info,
	     struct plugin_gcc_version *version)
{
#if VERSION_CHECK
  if (!plugin_default_version_check (version, &gcc_version))
    return 1;
#endif
  
  register_callback ("pass execution callback", PLUGIN_PASS_EXECUTION,
		     pass_execution_callback, NULL);
  return 0;
}
